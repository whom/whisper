(ns whisper.signald
  (:require [clojure.data.json     :as json]
            [clojure.tools.logging :as log])
  (:import
   (org.newsclub.net.unix AFUNIXSocket AFUNIXSocketAddress)
   (java.io File)))

(def callback-table (atom {}))

(defn get-signald-sock
  [filepath]
  (try
    (let [target (AFUNIXSocketAddress. (File. filepath))]
      (log/info "Connected to signald at: " filepath)
      (AFUNIXSocket/connectTo target))
    (catch Exception e
      (log/error e "Caught exception connecting to socket")
      nil)))

(defn assoc-callback-type
  [callback-token callback]
  (swap! callback-table assoc callback-token callback))

(defn disassoc-callback-type
  [callback-token]
  (swap! callback-table dissoc callback-token))

;; TODO wrap attempt to call callback func in try catch
(defn callback-loop
  [BufferedReader]
  (doall (for [line (line-seq BufferedReader)]
           (let [obj (json/read-str line)
                 type-tok (get obj "type")
                 callback-func (get @callback-table type-tok)]
             (log/info "Received message from signald (type: " type-tok ").")
             (if (nil? callback-func)
               (log/error "No callback for message type: " type-tok)
               (callback-func obj))))))

(defn get-version-from-version-message
  [version-message-obj]
  (let [data (get version-message-obj "data")]
    (str (get data "name")    " "
         (get data "version") " "
         (get data "branch"))))

(defn get-id-from-contact-or-group
  [contact]
  (let [c-id (get-in contact ["address" "number"])
        g-id (get-in contact ["groupId"])]
    (if (nil? c-id)
      (if (nil? g-id)
        (log/error "Document had no contact id or group id")
        g-id)
      c-id)))

;; sends data to signal socket
;; expects data to be a map
;; adds a newline
(defn make-req
  [output-stream data]
  (try (.write output-stream (str (json/write-str data) "\n")) (.flush output-stream)
       (catch Exception e (log/error e "Error sending to signald"))))
