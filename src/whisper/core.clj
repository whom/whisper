(ns whisper.core
  "Whisper Namespace"
  (:require [clojure.tools.logging :as log]
            [clojure.data.json     :as json]
            [clojure.core.async    :as async
             :refer [<!! thread]]
            [clojure.java.io       :as io])

  (:import (java.io BufferedReader InputStreamReader))

  (:use [whisper.ui]
        [whisper.signald]
        [whisper.storage]))

;; TODO: config management
;; TODO: make config dir
(def dbpath   "/tmp/temp_db")
(def sockpath "/var/run/signald/signald.sock")
(def user (atom {}))
(def link-status (atom false))

;; TODO: handle multi users (with list ui)
(defn handle-users
  [sigd data]
  (paint-user-select
   (get-in data ["data" "accounts"])
   true
   (fn [x _]
     ;; TODO: dont magic string this
     (if (= (get x "username") "Link New User")
       (make-req sigd {"type" "link"})
       (do (reset! user x)
           (init-user-schema (get x "username")))))))

(defn digest-group-or-contact-list
  [list db]
  ;; Should we iter in a background thread?
  (doseq [contact list]
    (let [id (get-id-from-contact-or-group contact)
          name (get contact "name")]
    (store-contact db (get @user "username") id contact)
    (add-json-contact {:id id :name name}))))

(defn -main
  "packs and shows main window with signald version string"
  [& args]

  ;; paint main chat window
  (paint-main)

  ;; prep to read signald
  ;; also open storage
  (let [sigd (get-signald-sock sockpath)
        db   (init-db          dbpath)]

    ;; check if signald conn failed
    (if (nil? sigd)
      (add-str-flag "No signald connection."))

      ;; register callbacks between the UI and signald
      (assoc-callback-type "version" (fn [x] (add-str-flag (get-version-from-version-message x))))
      (assoc-callback-type "linking_uri" (fn [x] (paint-linking x)))
      (assoc-callback-type "linking_successful" (fn [_] (paint-linking nil)
                                                  (reset! link-status true)))
      (assoc-callback-type "linking_error" (fn [_] (paint-linking nil)
                                             (log/error "Linking failed")
                                             (add-str-flag "Linking failed")
                                             (reset! link-status true)))
      (assoc-callback-type "unexpected_error" (fn [x] (add-str-flag (str "Unexpected error: "
                                                                         (get-in x ["data" "message"])))))
      (assoc-callback-type "contacts_list" (fn [x] (digest-group-or-contact-list
                                                    (get-in x ["data"]) db)))
      (assoc-callback-type "group_list" (fn [x] (digest-group-or-contact-list
                                                  (get-in x ["data" "groups"]) db)))

      ;; get io channels for signald
      (let [sigd-input  (io/writer (.getOutputStream sigd))
            sigd-output (BufferedReader. (InputStreamReader. (.getInputStream sigd)))
            sigd-loop   (thread (callback-loop sigd-output))]

        ;; handle login basically
        (assoc-callback-type "account_list" (fn [x] (handle-users sigd-input x)))

        ;; log in every time user state changes
        ;; another way we could do this is to have a callback triggered on a
        ;; signald message type of "subscribed"
        ;; but I thought it would be more efficient to just watch for results
        ;; from the call to handle-users
        ;; ALSO: close account picker
        (add-watch user :login-manager
                   (fn [key atom old-state new-state]
                     (let [old-user (get old-state "username")
                           new-user (get new-state "username")]

                       (if (not= old-user new-user)
                         (do (log/info key " changed state from "
                                       old-user " to " new-user)

                             ;; unsub from old acc
                             (if (some? old-user)
                               ;; TODO: make this a, do statement
                               ;; add a call to reset the UI
                               (make-req sigd-input ("type" "unsubscribe"
                                                     "username" old-user)))

                             ;; sub to new account
                             (if (some? new-user)
                               (do
                                 (make-req sigd-input {"type" "subscribe"
                                                       "username" new-user})
                                 (make-req sigd-input {"type" "sync_contacts"
                                                       "username" new-user})
                                 (make-req sigd-input {"type" "list_contacts"
                                                       "username" new-user})
                                 (make-req sigd-input {"type" "list_groups"
                                                       "username" new-user})
                                 ;; TODO: load messages or something
                                 ;; maybe wait till here to paint main UI
                                 (add-str-flag (str "Now logged in as " new-user))))

                             ;; close account picker
                             (paint-user-select [] false (fn [_ _] (log/error "this cannot be called"))))))))

        ;; whenever a link account operation finishes, retrigger handle-users
        (add-watch link-status :linking-manager
                   (fn [key atom old-state new-state]
                     (if (and (not= old-state new-state) new-state)
                       (do
                         (log/info key " has detected a link account operation finished.")
                         (reset! link-status false)
                         (make-req sigd-input {"type" "list_accounts"})))))

        ;; find avail accounts
        (make-req sigd-input {"type" "list_accounts"})

        ;; finally, block on signald loop till return
        (<!! sigd-loop))))
