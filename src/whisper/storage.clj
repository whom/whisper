(ns whisper.storage
  "Whisper storage module"
  (:require [clojure.tools.logging :as log]
            [codax.core            :as c]))

(defn init-db
  "opens db at filepath and makes sure schema is in place"
  [filepath]
  (let [db (c/open-database! filepath)]
    ;; TODO: schema preperations here
    db))

(defn init-user-schema
  [db username]
  (let [user-doc (c/get-at! db [username])
        user-contact-doc (get user-doc :contacts)
        user-messages-doc (get user-doc :messages)]

    (if (nil? user-doc)
      (c/assoc-at! db [username] {:contacts {}
                                  :messages {}}))
    (if (nil? user-contact-doc)
      (c/assoc-at! db [username :contacts] {}))

    (if (nil? user-messages-doc)
      (c/assoc-at! db [username :messages] {}))))

(defn store-contact
  "adds a new contact to db"
  [db username contact-id contact-doc]
  (let [curr-doc (c/get-at! db [username :contacts contact-id])]
    (if (or (nil? curr-doc) (not= curr-doc contact-doc))
      (c/assoc-at! db [username :contacts contact-id] contact-doc)))
  (if (nil? (c/get-at! db [username :messages contact-id]))
    (c/assoc-at! db [username :messages contact-id] [])))

(defn store-message
  "adds a message to the db"
  [db username contact-id message-doc]
  (let [curr-thread (c/get-at! db [username :messages contact-id])]
    ;; no contact? dont bother
    (if (nil? curr-thread)
      (log/info "Received message for undiscovered contact (wont store).")
      (c/merge-at! db [username :messages contact-id] [message-doc]))))

(defn get-contacts
  "retrieve all contacts"
  [db username]
  (c/get-at! db [username :contacts]))

(defn get-contact
  "retrieve a specific contact"
  [db username contact-id]
  (c/get-at! db [username :contacts contact-id]))

(defn get-thread
  "get all messages to and from a specific contact/group"
  [db username contact-id]
  (c/get-at! db [username :messages contact-id]))

(defn get-thread-and-contact-if-contact
  "get all messages to and from a contact, as well as their contact info IF they are a stored contact"
  [db username contact-id]
  (let [contact-doc (c/get-at! db [username :contacts contact-id])]
    (if (not (nil? contact-doc))
      ;; contact exists
      {:contact contact-doc :thread (c/get-at! db [username :messages contact-id])}
      ;; else return nil
      nil)))
