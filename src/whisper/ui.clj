(ns whisper.ui
  "Whisper UI Module"
  (:require [cljfx.api             :as fx]
            [clojure.data.json     :as json]
            [clojure.tools.logging :as log]
            [clojure.core.cache    :as cache]
            [clj.qrgen             :as qr]
            [clojure.java.io       :as io])

  (:import [net.glxn.qrgen.core.image ImageType]
           [java.io FileInputStream]
           [javafx.scene.image Image WritableImage]))

;; simple permutation of json into label
(defn paint-message
  [message]
  {:fx/type :label
   :text (json/write-str message)})

(defn paint-contact
  [contact]
  {:fx/type :v-box
   :children [{:fx/type :label
               :scale-x 1.1
               :text (get contact "name")}
              {:fx/type :label
               :scale-x 0.9
               :text (get contact "id")}]})

(defn paint-flag
  [flag]
  {:fx/type :label
   :v-box/margin 10
   :text (json/write-str flag)})

;; atomic global state for the UI
(def main-context  (atom (fx/create-context
                          {:contacts []
                           :messages []
                           :flags    []}
                         cache/lru-cache-factory)))

(def qrcode-context (atom (fx/create-context
                           {:message "In Progress..." }
                           cache/lru-cache-factory)))

(defn add-json-message
  [obj]
  (swap! main-context fx/swap-context update :messages conj (paint-message obj)))

(defn add-json-contact
  [obj]
  (swap! main-context fx/swap-context update :contacts conj (paint-contact obj)))

(defn add-str-flag
  [str]
  (swap! main-context fx/swap-context update :flags conj
         (paint-flag str)))

;; TODO: maybe an actual UI lol
(defn main-ui
  [{:keys [fx/context]}]
  {:fx/type :stage
   :showing true
   :width 500
   :height 500
   :scene {:fx/type :scene
           :root {:fx/type :v-box
                  :children (conj (fx/sub-val context :flags)
                                   {:fx/type :h-box
                                     :children [{:fx/type :v-box
                                                 :children (fx/sub-val context :contacts)}
                                                ;; TODO: scrollable view (list view?)
                                                {:fx/type :v-box
                                                 :children (fx/sub-val context :messages)}
                                                ;; TODO: compose message view
                                                ]})}}})

(defn qrcode-ui
  [{:keys [showing image]}]
  {:fx/type :stage
   :showing showing
   :title "link device"
   :width 500
   :height 500
   :scene {:fx/type :scene
           :root {:fx/type :v-box
                  :children [{:fx/type :image-view
                              :fit-height 480
                              :fit-width 480
                              :image image}]}}})

(defn user-select-ui
  [{:keys [users callback showing]}]
  {:fx/type :stage
   :showing showing
   :title "select a user"
   :width 200
   :height 500
   :scene {:fx/type :scene
           :root {:fx/type :v-box
                  :children [{:fx/type :label
                              :v-box/margin 5
                              :text "Pick a user to continue"}
                             {:fx/type fx/ext-let-refs
                              :refs {::toggle-group {:fx/type :toggle-group}}
                              :desc {:fx/type :v-box
                                     :padding 20
                                     :spacing 10
                                     :children (for [user users]
                                                 {:fx/type :radio-button
                                                  :toggle-group {:fx/type fx/ext-get-ref
                                                                 :ref ::toggle-group}
                                                  :selected false
                                                  :text (get user "username")
                                                  :on-action #(callback user %)})}}]}}})

(def main-renderer (fx/create-renderer
                    :middleware (comp
                                 fx/wrap-context-desc
                                 (fx/wrap-map-desc (fn [_] {:fx/type main-ui})))
                    :opts {:fx.opt/type->lifecycle #(or (fx/keyword->lifecycle %)
                                                        (fx/fn->lifecycle-with-context %))}))

(def aux-renderer (fx/create-renderer))

(defn paint-main
  []
  (fx/mount-renderer main-context main-renderer))

(defn paint-linking
  [obj]
  (if (nil? obj)
    ;; if no obj, turn off window
    (aux-renderer {:fx/type qrcode-ui
                   :showing false
                   :image   (WritableImage. 1 1)
                   :message "lol"})

    ;; else gen qr
    (let [uri (str (get-in obj ["data" "uri"]))
          image (io/file (qr/from uri))]
      (aux-renderer {:fx/type qrcode-ui
                     :showing true
                     :image   (Image. (FileInputStream. image))}))))

;; TODO: modify callback into event handler
(defn paint-user-select
  [users showing callback]
  (aux-renderer
   {:fx/type user-select-ui
    :showing showing
    ;; "Make New User" is a magic token. see handle-users in core
    ;; TODO: dont
    :users (conj users {"username" "Link New User"})
    :callback callback}))
